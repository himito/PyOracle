=============================================
PyOracle - Python Factor/Audio Oracle Library
=============================================
Under development by the Center for Research in Entertainment and Learning
(CREL) @UCSD


Contacts
--------
Greg Surges, surgesg@gmail.com
Cheng-i Wang, wangsix@gmail.com


About
-----
PyOracle is a Python library for machine musical improvisation and analysis
in the family of software built around the Factor Oracle and Audio Oracle algorithms.
One of the main innovations in PyOracle is using functions related to
Music Information Dynamics for tuning of sound models with multiple sound features.
A symbolic version also allows working with MusicXML and use of
musical rules for controlling the generation of sound.


Dependencies
------------
Numpy, Scipy, Matplotlib &
Bregman Audio-Visual Information Toolbox:
http://digitalmusics.dartmouth.edu/bregman/


Installation
------------
run python setup.py install
