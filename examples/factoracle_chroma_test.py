
# coding: utf-8

## Audio Oracle and HMM 

# The purpose of this demo is to show how audio oracle could infer latent variables the same as hidden markov model, the differernce between them and the potential advantage of using audio oracle over HMM. 
# 
# In short, the audio oracle learns a temporal structure of a time series given threshold. We will show that by using information rate as an objective function, the best threshold for a time series could be found, and with the best threshold the correct structure of the time series could be inferred without specifying the number of latent variables, which an HMM algorithm will require.

### Chromagram and $L_2$-norm between each Chromagram Frame

# In[1]:

# %load_ext autoreload
# %autoreload 2
import factoracle
import numpy as np
import matplotlib.pyplot as plt
import Resources.analysis as an
from bregman.suite import Chromagram
from sklearn.hmm import GaussianHMM

get_ipython().magic(u'matplotlib inline')

filename = 'hmmtesttone.wav'
fft_size = 8192
hop_size = fft_size

chroma = Chromagram(filename, log10=True, intensify=True, nfft=fft_size, wfft=fft_size, nhop=hop_size)

plt.figure()
plt.imshow(chroma.X, aspect = 'auto', origin = 'lower', interpolation = 'nearest', cmap ='Greys')
plt.title('Chromagram', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Chroma Bin(1 = C, 3 = E ...)', fontsize = 14)

plt.figure()
plt.stem(np.sqrt(np.sum(np.diff(chroma.X)**2,0)))
plt.grid(b = 'on')
plt.xlim((0, 55))
plt.title('2-norm between Chroma frames', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('2-norm', fontsize = 14)


### Calculate Threshold Value using IR

# In[2]:

features = {}
frames_per = 1
chroma_frames = [[0]] * len(chroma.X[0])
for i, frame in enumerate(chroma.X[0]):
    chroma_frames[i] = [component[i] for component in chroma.X]
features['chroma'] = chroma_frames

r = (0.0000, 0.05, 0.005) 
ideal_t = factoracle.calculate_ideal_threshold(r, features, 'chroma', frames_per_state=frames_per, ir_type='cum')
x_a = [i[1] for i in ideal_t[1]]
y_a = [i[0] for i in ideal_t[1]]
plt.figure()
plt.plot(x_a, y_a, linewidth = 2)
plt.title('IR vs. Threshold Value', fontsize = 18)
plt.grid(b = 'on')
plt.xlabel('Threshold', fontsize = 14)
plt.ylabel('IR', fontsize = 14)


### Construct Audio Oracle

# In[3]:

best_oracle = factoracle.make_oracle(ideal_t[0][1], 'a', features, 'chroma', frames_per_state=frames_per)
img = factoracle.draw_oracle(best_oracle, 'boutput.png', size=(1200, 400))
plt.figure()
f = plt.gca()
plt.imshow(np.asarray(img), aspect = 'auto')
f.axes.xaxis.set_ticklabels([])
f.axes.yaxis.set_ticklabels([])
plt.title('Oracle Structure', fontsize = 18)


### Number of Latent Variables Found and IR over the Signal

# In[4]:

best_oracle.get_num_symbols()


# In[5]:

ir, code, compror = factoracle.calculate_ir(best_oracle, 0, ir_type='old')
ir = np.array(ir)
ir = (ir / ir.max())*10.8

plt.figure()
plt.imshow(chroma.X, aspect = 'auto', origin = 'lower', cmap = 'Greys', interpolation = 'nearest')
plt.plot(np.linspace(0, len(ir)-1, len(ir)), ir, 'b', linewidth = 2)
plt.xlim((0, len(ir)-1))
plt.ylim((0,11))
plt.title('Information rate (IR) vs. Chromagram', fontsize = 18)
plt.xlabel('Frame', fontsize=14)
plt.ylabel('IR(Scaled)', fontsize=14)


### Latent Variables from AO against Signal

# In[6]:

latent = an.infer_latent_var(best_oracle)

plt.figure()
plt.imshow(chroma.X, aspect = 'auto', origin = 'lower', cmap = 'Greys', interpolation = 'nearest')
for i in range(len(latent)):
    plt.plot(np.array(latent[i])-1, ((i+1)*10.0/len(latent))*np.ones(len(latent[i],)),'o')
plt.xlim((0, len(ir)-1))
plt.ylim((0,11))
plt.title('Chromagram with hidden states(pyoracle)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Chroma bins and hidden states', fontsize = 14)


### Train an HMM 

# In[7]:

n_components = best_oracle.get_num_symbols()
model = GaussianHMM(n_components, 
                    covariance_type="tied", 
                    n_iter=1000,
                    )
model.fit([chroma.X.transpose()])
plt.figure()
f = plt.gca()
plt.imshow(model.means_.transpose(),aspect = 'auto', origin = 'lower', cmap = 'Greys', interpolation = 'nearest')
plt.title('Component mean for HMM with Guassian emission', fontsize =18)
plt.xlabel('Components', fontsize = 14)
plt.ylabel('Chroma bins', fontsize = 14)
f.axes.xaxis.set_ticklabels([])


### Latent Variables from HMM against Signal

# In[8]:

hidden_states = model.predict(chroma.X.transpose())
plt.figure()
T = np.arange(chroma.X.transpose().shape[0])
plt.imshow(chroma.X, aspect = 'auto', origin = 'lower', cmap = 'Greys', interpolation = 'nearest')
for i in range(n_components):
    # use fancy indexing to plot data in each state
    idx = (hidden_states == i)
    plt.plot(T[idx], (i+1)*10/n_components*np.ones(len(T[idx])), 'o', label="%dth hidden state" % i)
plt.xlim((0, len(ir)-1))
plt.ylim((0,11))
plt.title('Chromagram with hidden states(HMM)', fontsize = 18)
plt.xlabel('Frame', fontsize = 14)
plt.ylabel('Chroma bins and hidden states', fontsize = 14)


### Conclusion

# A straitforward takeaway of this demo is that both HMM and AO could find the correct structure of a given time series, but AO needs less a priori information regarding the time series while the exact number of latent variable has to be known a priori for HMM.
