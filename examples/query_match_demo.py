'''
Created on May 12, 2014

@author: Wang
'''
import factoracle as fa
import Resources.analysis as an
import Resources.generate as ge
import Resources.helpers as helpers
import os

def main():
    
    targetfile = os.path.abspath('') + '/FunkBass.wav'
    outfilename = os.path.abspath('') + '/audio_generate_test.wav'
    
    todo = ['chroma']
    buffer_size = 4096*4
    hop = buffer_size/2
    target_features = fa.make_features(targetfile, buffer_size, hop, todo)
    query_features = target_features
    query_events = helpers.features_to_events(query_features)
    
    thresholds = (0.001, 2.0, 0.01)
    ideal_t = fa.calculate_ideal_threshold(thresholds, target_features, 'chroma')
    thresh = ideal_t[0][1]
    
    t_oracle = fa.make_oracle(thresh, 'a', target_features, 'chroma')

    path_1, cost_1, i_hat_1 = an.query_complete(t_oracle, query_events)
    
    print '================='
    print 'Query with itself'
    print '================='
    print 'Perfect reconstruction:'
    if path_1[i_hat_1] == range(1,t_oracle.n_states):
        print 'Passed with cost '+ str(cost_1[i_hat_1]) 
    else:
        print 'Failed with cost '+ str(cost_1[i_hat_1])
        
    ge.audio_synthesis(targetfile, outfilename, path_1[i_hat_1], buffer_size, hop)
    
    test_features = fa.make_features(outfilename, buffer_size, hop, todo)
    
    thresholds2 = (0.001, 2.0, 0.01)
    ideal_t2 = fa.calculate_ideal_threshold(thresholds2, test_features, 'chroma')
    thresh2 = ideal_t2[0][1]
    
    test_oracle = fa.make_oracle(thresh2, 'a', test_features, 'chroma')
    s, _kend, _ktrace = ge.generate(test_oracle, 
                                    test_oracle.n_states, 
                                    0.25, 
                                    0, LRS = 1, 
                                    weight='weight')

    queryout = os.path.abspath('') + '/query_generate_test.wav'
    ge.audio_synthesis(outfilename, queryout, s, buffer_size, hop)
        
    query_features2 = fa.make_features(queryout, buffer_size, hop, todo)
    query_events2 = helpers.features_to_events(query_features2)
    
    path, cost, i_hat = an.query_complete(test_oracle, query_events2)
    print '============================'
    print 'Query with shuffled original'
    print '============================'
    print 'Perfect reconstruction:'
    if path[i_hat] == s:
        print 'Passed with cost '+ str(cost[i_hat]) 
    else:
        print 'Failed with cost '+ str(cost[i_hat])
        
if __name__ == '__main__':
    main()