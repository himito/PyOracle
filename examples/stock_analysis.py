
# coding: utf-8

## Stock Analysis with HMM and PyOracle

# In[12]:

get_ipython().magic(u'load_ext autoreload')
get_ipython().magic(u'autoreload 2')
from __future__ import division 
import datetime
import factoracle as fa
import Resources.analysis as an
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.finance import quotes_historical_yahoo
from matplotlib.dates import YearLocator, MonthLocator, DateFormatter
from sklearn.hmm import GaussianHMM

get_ipython().magic(u'matplotlib inline')

date1 = datetime.date(1995, 1, 1)  # start date
date2 = datetime.date(2014, 5, 28)  # end date
# get quotes from yahoo finance
quotes = quotes_historical_yahoo("INTC", date1, date2)
if len(quotes) == 0:
    raise SystemExit

# unpack quotes
dates = np.array([q[0] for q in quotes], dtype=int)
close_v = np.array([q[2] for q in quotes])
volume = np.array([q[5] for q in quotes])[1:]

# take diff of close value
# this makes len(diff) = len(close_t) - 1
# therefore, others quantity also need to be shifted
diff = close_v[1:] - close_v[:-1]
dates = dates[1:]
close_v = close_v[1:]

# pack diff and volume for training
X = np.column_stack([diff, volume])


### Gaussian HMM

# In[13]:

n_components = 5

# make an HMM instance and execute fit
model = GaussianHMM(n_components, covariance_type="diag", n_iter=1000)

model.fit([X])

# predict the optimal sequence of internal hidden state
hidden_states = model.predict(X)

years = YearLocator()   # every year
months = MonthLocator()  # every month
yearsFmt = DateFormatter('%Y')

fig = plt.figure()
ax = fig.add_subplot(111)
for i in range(n_components):
    # use fancy indexing to plot data in each state
    idx = (hidden_states == i)
    ax.plot_date(dates[idx], close_v[idx], 'o', label="%dth hidden state" % i)
ax.legend()

# format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)
ax.autoscale_view()

# format the coords message box
ax.fmt_xdata = DateFormatter('%Y-%m-%d')
ax.fmt_ydata = lambda x: '$%1.2f' % x
ax.grid(True)

fig.autofmt_xdate()


### PyOracle

# In[14]:

features = {}
frames_per = 1
stock_data = np.divide(np.add(X, -np.mean(X,0)), np.std(X,0))
features['diffnvolume'] = stock_data

r = (0.0000, 1.45, 0.05)
ideal_t = fa.calculate_ideal_threshold(r, features, 'diffnvolume', frames_per_state=frames_per, ir_type='cum')
x_a = [i[1] for i in ideal_t[1]]
y_a = [i[0] for i in ideal_t[1]]
plt.figure()
plt.plot(x_a, y_a, linewidth = 2)
plt.title('IR vs. Threshold Value', fontsize = 18)
plt.grid(b = 'on')
plt.xlabel('Threshold', fontsize = 14)
plt.ylabel('IR', fontsize = 14)


# In[15]:

best_oracle = fa.make_oracle(ideal_t[0][1], 'a', features, 'diffnvolume', frames_per_state=frames_per)
img = fa.draw_oracle(best_oracle, 'stock.png', size=(1440, 900))
plt.figure()
f = plt.gca()
plt.imshow(np.asarray(img), aspect = 'auto')
f.axes.xaxis.set_ticklabels([])
f.axes.yaxis.set_ticklabels([])
plt.title('Oracle Structure - Stock Data', fontsize = 18)


#### IR against Closing Volume

# In[16]:

ir, code, compror = fa.calculate_ir(best_oracle, 0, ir_type='old')
ir = np.array(ir)
ir = (ir / ir.max())*(close_v.max()*0.9)
latent = an.infer_latent_var(best_oracle)

fig2 = plt.figure()
ax = fig2.add_subplot(111)
ax.stem(dates, ir, 'r-')
ax.hold(b = 'on')
ax.plot_date(dates, close_v, '-')

# format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)
ax.autoscale_view()

# format the coords message box
ax.fmt_xdata = DateFormatter('%Y-%m-%d')
ax.fmt_ydata = lambda x: '$%1.2f' % x
ax.grid(True)
fig2.autofmt_xdate()


#### LRS value

# In[17]:

fig4 = plt.figure()
ax = fig4.add_subplot(111)
ax.plot_date(dates, best_oracle.lrs, '-')
# ax.hold(b = 'on')
# ax.plot_date(dates, close_v, '-')
# for i in range(len(latent)):
#     # use fancy indexing to plot data in each state
#     idx = latent[i]
#     ax.plot_date(dates[idx], close_v[idx], 'o', label="%dth hidden state" % i)
# ax.legend()

# format the ticks
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)
ax.autoscale_view()

# format the coords message box
ax.fmt_xdata = DateFormatter('%Y-%m-%d')
ax.fmt_ydata = lambda x: '$%1.2f' % x
ax.grid(True)
fig4.autofmt_xdate()


#### Self-similarity matrix using reverse suffix links

# In[18]:

rs = an.create_selfsim(best_oracle, method = 'rsfx')
fig3 = plt.figure()
ax = fig3.add_subplot(111)
ax.imshow(rs, origin = 'lower', cmap = 'Greys', interpolation = 'nearest', extent = [dates[0], dates[-1], dates[0], dates[-1]])
ax.xaxis.set_major_locator(years)
ax.xaxis.set_major_formatter(yearsFmt)
ax.xaxis.set_minor_locator(months)
ax.yaxis.set_major_locator(years)
ax.yaxis.set_major_formatter(yearsFmt)
ax.yaxis.set_minor_locator(months)
ax.autoscale_view()

# format the coords message box
ax.fmt_xdata = DateFormatter('%Y-%m-%d')
ax.fmt_ydata = DateFormatter('%Y-%m-%d')
fig3.autofmt_xdate()

